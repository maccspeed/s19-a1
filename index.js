
let x = 2;
let getCube = Math.pow(x, 3);
console.log(`The cube of ${x} is ${getCube}`);

const address = ["258", "Washington Ave NW", "California", "90011"];
const [block, street, city, zip] = address;
console.log(`I live at ${block} ${street}, ${city} ${zip}`);

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	mearsurement: "20 ft 3 in."
}
const {name, type, weight, mearsurement} = animal;
function getAnimal ({name, type, weight, mearsurement}) {
	console.log(`${name} was a ${type}. He weighed at ${weight} with a mearsurement of ${mearsurement}`)
}
getAnimal(animal);

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);